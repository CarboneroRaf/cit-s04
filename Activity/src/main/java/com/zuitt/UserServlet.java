package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7560556849380195433L;

	public void init() throws ServletException{
		System.out.println("******************************************");
		System.out.println(" InformationServlet has been initialized");
		System.out.println("******************************************");
	}
	
	
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		
		//First name - System Properties
		System.getProperties().put("fname", req.getParameter("fname").toString());
		
		//Last name - HttpSession
		HttpSession lname = req.getSession();
		lname.setAttribute("lname", req.getParameter("lname").toString());
		
		//Email - Servlet Context
		ServletContext email = getServletContext();
		email.setAttribute("email", req.getParameter("email").toString());
		
		//Contact Number - Url Rewrtiting via sendRedirect
		res.sendRedirect("details?contact="+req.getParameter("contact").toString());
	}
	
	
	
	public void destroy() {
		System.out.println("******************************************");
		System.out.println(" InformationServlet has been destroyed");
		System.out.println("******************************************");
	}
}
